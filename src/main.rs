use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{Value, json};

async fn function_handler(event: Value, _: Context) -> Result<Value, Error> {
    Ok(json!({ "message": "Hello from Rust!" }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(function_handler)).await?;
    Ok(())
}
