# Instrumenting a Rust Lambda Function with Logging and Tracing

## Introduction

This project focuses on instrumenting a Rust Lambda function with logging and tracing capabilities, integrating AWS X-Ray for tracing, and connecting logs and traces to CloudWatch for centralized monitoring and analysis.

## Project Requirements

1. **Add logging to a Rust Lambda function:** Implement logging within the Lambda function to track its behavior and performance.
2. **Integrate AWS X-Ray tracing:** Enhance the function with AWS X-Ray tracing to gain insights into its execution flow and performance.
3. **Connect logs/traces to CloudWatch:** Configure the function to send logs and traces to CloudWatch for effective monitoring.

## Step-by-Step Implementation

### Setting Up Rust Environment and Lambda Function

#### 1. Setting Up AWS Cloud9 Environment
Create and access your Cloud9 environment through the AWS Console.

#### 2. Installing Rust and Cargo
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
source $HOME/.cargo/env
rustc --version
cargo --version
```

#### 3. Creating a New Rust Lambda Function
```
cargo lambda new my_rust_lambda
cd my_rust_lambda
```

### Implementing Logging

#### 1. Add dependencies to Cargo.toml:
```
[dependencies]
log = "0.4"
simple_logger = "1.0"
```

#### 2. Initialize the logger in src/main.rs:
```
use simple_logger::SimpleLogger;
use log::LevelFilter;

fn main() {
    SimpleLogger::new().with_level(LevelFilter::Info).init().unwrap();
    log::info!("Logger initialized.");
}
```

##### 3. Add log statements in function handler:
```
log::info!("Function execution started");
log::error!("An error occurred");
```
After deploymeng of lambda function, we can watch from AWS lambda platform as:
![Lambda Function](Pictures/lambda.png)

After we testing our lambda function, we can see our X-Ray has traced our running of lambda function.
![Lambda testing](Pictures/Lambda_testing.png)

### Integrating AWS X-Ray Tracing
Currently, Rust does not have a direct AWS X-Ray SDK. You would need to use a workaround or manually create segments and subsegments. Here is how we interact with X-Ray:
```
// Pseudo-code - Replace with actual logic for Rust
fn handler(event: LambdaEvent, context: LambdaContext) {
    xray::begin_segment("LambdaFunctionSegment");

    // Your function logic
    log::info!("Processing event");

    xray::end_segment();
}
```
By calling lambda testing on the AWS Lambda, we can see from AWS X-ray that:
![X-ray log group details](Pictures/Cloudwatch.png)

We can also watch the tracing history of the past 6 hours. 
![X-ray trace history 6 hours](Pictures/Trace.png)
![X-ray trace history 6 hours](Pictures/Trace2.png)


### Connecting Logs/Traces to CloudWatch
Ensure the Lambda function's IAM role has the necessary permissions. No specific code is required here; just configure Lambda function to send logs and traces to CloudWatch in the AWS Console or via AWS CL
```
aws lambda update-function-configuration --function-name my_rust_lambda --tracing-config Mode=Active
```
After clicking into the lambda function we set, we can watch its corresponding log stream.
![X-ray log group details](Pictures/XRay_Lambda.png)





